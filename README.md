# A computer counting on fingers

A computer vision class project aiming to be able to count the number of outstretched fingers on a picture of a hand. 

## Installation 

Once the project is cloned, please execute the following command at the root of the project to set up your environment : 
``` pip install -r requirement.txt```

You should now be able to execute any of the notebooks or to use some of the modules fonctions.

## Project structure 

Our project is built with a rather simple structure involving three folders : 
- the ```./data/``` folder containing all the images we used to build and test our functions, divided in 5 directories with different specificities detailed in the report 
- the ```./modules/``` folder containing the final implementations of our functions in four files : 
    - ```detect_fingers_from_masks.py``` containing the functions to count the fingers on a mask containing the hand(s)
    - ```skin_detection_generic.py``` containing our generic function for skin detection that seems best suited for any kind of images
    - ```skin_detection_specific.py```containing a slightly different function for skin detection with some preprocessing on the masks used that allow for greater precision on our use case for some specific conditions of framing, lighting and size
    - ```utils.py``` containing all the generic functions we built to gain some time or facilitate our experimentations and implementations 
- the ```./notebooks```containing three Jupyter notebooks. The two first are dedicated to the experimentations we conducted to build our approach and the final Python modules written while the third is used to test our results by using directly the modules functions. 

For any further detail on our approach our result, please refer to the report accompagnying the project. 